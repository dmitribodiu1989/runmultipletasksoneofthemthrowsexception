﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RunMultipleTasksAndThrowException
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            try
            {
                var catTask = Task.Run(FeedCat);
                var houseTask = SellHouse();
                var carTask = BuyCar();

               var tasksResults = new List<string>();

                var results = await WhenAllOrException(new List<Task<string>> {carTask, houseTask, catTask});
                
                //var cat = await catTask;
                //var house = await houseTask;
                //var car = await carTask;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            Console.ReadKey();
        }

        private static async Task<string> BuyCar()
        {
            await Task.Delay(2000);
            return "BuyCarResult";
        }

        private static async Task<string> SellHouse()
        {
            await Task.Delay(2000);
            //throw new ArgumentException("FeedCat validation exception");
            return "SellHouseResult";
        }

        private static Task<string> FeedCat()
        {
            throw new ArgumentException("FeedCat validation exception");


        }



        public static Task<ResultOrException<T>[]> WhenAllOrException<T>(IEnumerable<Task<T>> tasks)
        {
            return Task.WhenAll(tasks.Select(WrapResultOrException));
        }

        private static async Task<ResultOrException<T>> WrapResultOrException<T>(Task<T> task)
        {
            try
            {
                var result = await task;
                return new ResultOrException<T>(result);
            }
            catch (ArgumentException ex)
            {
                return new ResultOrException<T>(ex);
            }
        }


        public class ResultOrException<T>
        {
            public ResultOrException(T result)
            {
                IsSuccess = true;
                Result = result;
            }

            public ResultOrException(Exception ex)
            {
                IsSuccess = false;
                Exception = ex;
            }

            public bool IsSuccess { get; }
            public T Result { get; }
            public Exception Exception { get; }
        }
    }
}
